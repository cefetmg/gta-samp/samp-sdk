#include "samp-sdk/exception/cell-param-index-out-of-bounds.hpp"
#include <string>
#include <sstream>

using namespace std;
using namespace sampsdk;

CellParamIndexOutOfBounds::CellParamIndexOutOfBounds(AMX *t_amxContext, const cell *t_cellParams, int t_paramsCount, int t_paramIndex):
    out_of_range(buildErrorMessage(t_cellParams, t_paramsCount, t_paramIndex)),
    m_amxContext(t_amxContext), 
    m_cellParams(t_cellParams), 
    m_paramsCount(t_paramsCount), 
    m_paramIndex(t_paramIndex) {}

CellParamIndexOutOfBounds::~CellParamIndexOutOfBounds() {}

const AMX * CellParamIndexOutOfBounds::amxContext() const { return m_amxContext; }

const cell * CellParamIndexOutOfBounds::cellParams() const { return m_cellParams; }

int CellParamIndexOutOfBounds::paramsCount() const { return m_paramsCount; }

int CellParamIndexOutOfBounds::paramIndex() const { return m_paramIndex; }

const string CellParamIndexOutOfBounds::buildErrorMessage(const cell *t_cellParams, int t_paramsCount, int t_paramIndex) const {
    stringstream stringStream;

    stringStream << "Attempt to access invalid index in cell parameter array:";
    stringStream << ' ' << "paramsCount=" << t_paramsCount;
    stringStream << ',' << "paramIndex=" << t_paramIndex;
    stringStream << ',' << "cellParams={";

    if (t_paramsCount > 0) {
        stringStream << t_cellParams[0];
    }

    for (int i = 1; i < t_paramsCount; i++) {
        stringStream << ',' << t_cellParams[i];
    }

    stringStream << '}';

    return stringStream.str();
}
