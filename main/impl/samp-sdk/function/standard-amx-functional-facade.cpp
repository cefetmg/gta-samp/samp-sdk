#include "samp-sdk/function/standard-amx-functional-facade.hpp"
#include "samp-sdk/function/amx-indexed-function.hpp"
#include <memory>

using namespace std;
using namespace sampsdk;

StandardAMXFunctionalFacade::StandardAMXFunctionalFacade(AMX *t_amxContext): m_amxContext(t_amxContext) { }

StandardAMXFunctionalFacade::~StandardAMXFunctionalFacade() { }

inline shared_ptr<AMXFunction> StandardAMXFunctionalFacade::getNativeFunction(const string &name) const {
    int index;
    amx_FindNative(m_amxContext, name.c_str(), &index);
    return shared_ptr<AMXFunction>(new AMXIndexedFunction((AMX *) m_amxContext, index, name));
}

inline shared_ptr<AMXFunction> StandardAMXFunctionalFacade::getPublicFunction(const string &name) const {
    int index;
    amx_FindPublic(m_amxContext, name.c_str(), &index);
    return shared_ptr<AMXFunction>(new AMXIndexedFunction((AMX *) m_amxContext, index, name));
}