#include "samp-sdk/function/amx-indexed-function.hpp"
#include <list>

using namespace std;
using namespace sampsdk;

AMXIndexedFunction::AMXIndexedFunction(AMX *t_amxContext, int t_functionIndex, string t_functionName): 
    m_amxContext(t_amxContext), m_functionIndex(t_functionIndex), m_functionName(t_functionName) {}

AMXIndexedFunction::~AMXIndexedFunction() {}

cell AMXIndexedFunction::operator () (const std::list<cell> params) const {
    for (auto it = params.rbegin(); it != params.rend(); it++) {
        amx_Push(m_amxContext, *it);
    }
    cell returnValue;
    amx_Exec(m_amxContext, &returnValue, m_functionIndex);
    return returnValue;
}