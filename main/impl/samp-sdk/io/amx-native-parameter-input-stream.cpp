#include "samp-sdk/io/amx-native-parameter-input-stream.hpp"
#include "samp-sdk/exception/cell-param-index-out-of-bounds.hpp"
#include <iostream>

using namespace std;
using namespace sampsdk;

inline int count_parameters(const cell *initialPosition) {
    return initialPosition[0]/sizeof(cell);
}

AMXNativeParameterInputStream::AMXNativeParameterInputStream(AMX *t_amxContext, cell *t_cellArray): 
    StandardAMXCellInputStream(t_amxContext, t_cellArray),
    m_cellInitialArray(t_cellArray),
    m_parametersCount(count_parameters(m_cellInitialArray)),
    m_currentIndex(0) {

    shiftCellPointer();
}

void AMXNativeParameterInputStream::shiftCellPointer() {
    m_currentIndex++;
    StandardAMXCellInputStream::shiftCellPointer();
}

CellInputStream & AMXNativeParameterInputStream::operator >> (int &t_targetInt) {
    assertIndexes();
    return StandardAMXCellInputStream::operator >> (t_targetInt);
}

CellInputStream & AMXNativeParameterInputStream::operator >> (float &t_targetFloat) {
    assertIndexes();
    return StandardAMXCellInputStream::operator >> (t_targetFloat);
}

CellInputStream & AMXNativeParameterInputStream::operator >> (char &t_targetChar) {
    assertIndexes();
    return StandardAMXCellInputStream::operator >> (t_targetChar);
}

CellInputStream & AMXNativeParameterInputStream::operator >> (std::string &t_targetString) {
    assertIndexes();
    return StandardAMXCellInputStream::operator >> (t_targetString);
}

CellInputStream & AMXNativeParameterInputStream::operator >> (cell * &t_targetReference) {
    assertIndexes();
    return StandardAMXCellInputStream::operator >> (t_targetReference);
}

void AMXNativeParameterInputStream::assertIndexes() {
    if (m_currentIndex > m_parametersCount) {
        throw CellParamIndexOutOfBounds(m_amxContext, m_cellInitialArray, m_parametersCount, m_currentIndex);
    }
}