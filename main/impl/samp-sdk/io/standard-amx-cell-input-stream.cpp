#include "samp-sdk/io/standard-amx-cell-input-stream.hpp"
#include <memory>
#include <stdexcept>

using namespace std;
using namespace sampsdk;

StandardAMXCellInputStream::StandardAMXCellInputStream(AMX *t_amxContext, cell *t_cellPointer): CellInputStream::CellInputStream(t_amxContext, t_cellPointer) {
    if (t_amxContext == nullptr) {
        throw invalid_argument("Provided a null value instead of AMX context to create a cell stream reader");
    }
    if (t_cellPointer == nullptr) {
        throw invalid_argument("Provided a null cell reference to open an input stream");
    }
}

StandardAMXCellInputStream::~StandardAMXCellInputStream() {}

CellInputStream & StandardAMXCellInputStream::operator >> (int &t_targetInt) {
    t_targetInt = *m_cellPointer;
    shiftCellPointer();
    return (*this);
}

CellInputStream & StandardAMXCellInputStream::operator >> (float &t_targetFloat) {
    t_targetFloat = amx_ctof(*m_cellPointer);
    shiftCellPointer();
    return (*this);
}

CellInputStream & StandardAMXCellInputStream::operator >> (char &t_targetChar) {
    t_targetChar = (char) *m_cellPointer;
    shiftCellPointer();
    return (*this);
}

CellInputStream & StandardAMXCellInputStream::operator >> (std::string &t_targetString) {
    cell *stringsInitialAddress;
    int stringLength;

    amx_GetAddr(m_amxContext, *m_cellPointer, &stringsInitialAddress);
    amx_StrLen(stringsInitialAddress, &stringLength);

    const int constStringLength = stringLength;

    char *destinationString = new char[constStringLength + 1];
    amx_GetString(destinationString, stringsInitialAddress, 0, stringLength + 1);

    t_targetString = destinationString;

    delete[] destinationString;
    shiftCellPointer();
    return (*this);
}

CellInputStream & StandardAMXCellInputStream::operator >> (cell * &t_targetReference) {
    amx_GetAddr(m_amxContext, *m_cellPointer, &t_targetReference);
    shiftCellPointer();
    return (*this);
}

inline void StandardAMXCellInputStream::shiftCellPointer() {
    m_cellPointer++;
}