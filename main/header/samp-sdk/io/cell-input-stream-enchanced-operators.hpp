#ifndef CELL_INPUT_STREAM_ENHANCED_OPERATORS_HPP
#define CELL_INPUT_STREAM_ENHANCED_OPERATORS_HPP

#include "samp-sdk/io/cell-input-stream.hpp"
#include <memory>

namespace sampsdk {

    template <typename T>
    std::shared_ptr<CellInputStream> operator >> (std::shared_ptr<CellInputStream> t_inputStream, T &t_targetReference);
}

#include "samp-sdk/io/cell-input-stream-enchanced-operators.hpp"

#endif