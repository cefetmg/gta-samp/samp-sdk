#ifndef STANDARD_AMX_CELL_INPUT_STREAM_HPP
#define STANDARD_AMX_CELL_INPUT_STREAM_HPP

#include "samp-sdk/io/cell-input-stream.hpp"

namespace sampsdk {

    class StandardAMXCellInputStream : public CellInputStream { 

        protected:

            virtual void shiftCellPointer();

        public:

            StandardAMXCellInputStream(AMX *t_amxContext, cell *t_cellArray);

            virtual ~StandardAMXCellInputStream();

            virtual CellInputStream& operator >> (int &t_targetInt) override;

            virtual CellInputStream& operator >> (float &t_targetFloat) override;

            virtual CellInputStream& operator >> (char &t_targetChar) override;

            virtual CellInputStream& operator >> (std::string &t_targetString) override;

            virtual CellInputStream& operator >> (cell * &t_targetReference) override;

    };

}

#endif