#ifndef AMX_CELL_INPUT_STREAM_HPP
#define AMX_CELL_INPUT_STREAM_HPP

#include "samp-sdk/amx/amx.h"
#include <vector>
#include <string>

namespace sampsdk {

    class CellInputStream {

        protected:

            AMX *m_amxContext;

            cell *m_cellPointer;

        public:

            CellInputStream(AMX *t_amxContext, cell *t_cellPointer): m_amxContext(t_amxContext), m_cellPointer(t_cellPointer) {}

            virtual ~CellInputStream() {}

            virtual CellInputStream & operator >> (int &t_targetInt) = 0;

            virtual CellInputStream & operator >> (float &t_targetFloat) = 0;

            virtual CellInputStream & operator >> (char &t_targetChar) = 0;

            virtual CellInputStream & operator >> (std::string &t_targetString) = 0;

            virtual CellInputStream & operator >> (cell * &t_targetReference) = 0;

    };

}

#endif