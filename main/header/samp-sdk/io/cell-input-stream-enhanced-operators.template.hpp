#ifndef CELL_INPUT_STREAM_ENHANCED_OPERATORS_TEMPLATE_HPP
#define CELL_INPUT_STREAM_ENHANCED_OPERATORS_TEMPLATE_HPP

#include "samp-sdk/io/cell-input-stream.hpp"
#include "samp-sdk/io/cell-input-stream-enchanced-operators.hpp"
#include <memory>

namespace sampsdk {

    template <typename T>
    std::shared_ptr<CellInputStream> operator >> (std::shared_ptr<CellInputStream> t_cellInputStream, T &t_targetReference) {
        (*t_cellInputStream) >> t_targetReference;
        return t_cellInputStream;
    }

}

#endif