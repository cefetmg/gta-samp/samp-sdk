#ifndef AMX_NATIVE_PARAMETER_INPUT_STREAM_HPP
#define AMX_NATIVE_PARAMETER_INPUT_STREAM_HPP

#include "samp-sdk/amx/amx.h"
#include "samp-sdk/io/standard-amx-cell-input-stream.hpp"

namespace sampsdk {

    class AMXNativeParameterInputStream : public  StandardAMXCellInputStream {

        private:

            const cell *m_cellInitialArray;

            const int m_parametersCount;

            int m_currentIndex;

        protected:

            virtual void shiftCellPointer() override;

            virtual void assertIndexes();

        public:

            AMXNativeParameterInputStream(AMX *t_amxContext, cell *t_cellArray);

            virtual ~AMXNativeParameterInputStream() {}

            virtual CellInputStream& operator >> (int &t_targetInt) override;

            virtual CellInputStream& operator >> (float &t_targetFloat) override;

            virtual CellInputStream& operator >> (char &t_targetChar) override;

            virtual CellInputStream& operator >> (std::string &t_targetString) override;

            virtual CellInputStream& operator >> (cell * &t_targetReference) override;

    };

}


#endif