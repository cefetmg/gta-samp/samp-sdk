#ifndef AMX_FUNCTION_HANDLER_HPP
#define AMX_FUNCTION_HANDLER_HPP

#include "samp-sdk/amx/amx.h"
#include <list>

namespace sampsdk {

class AMXFunction {

    public:

        virtual ~AMXFunction() {}

        virtual cell operator()(const std::list<cell> params) const = 0;

};

}


#endif