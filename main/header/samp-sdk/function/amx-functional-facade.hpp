#ifndef AMX_FACADE_HPP
#define AMX_FACADE_HPP

#include "samp-sdk/amx/amx.h"
#include "samp-sdk/function/amx-function.hpp"
#include <string>
#include <memory>

namespace sampsdk {

class AMXFunctionalFacade {

    public:

        virtual ~AMXFunctionalFacade() {};

        virtual inline std::shared_ptr<AMXFunction> getNativeFunction(const std::string &name) const = 0;

        virtual inline std::shared_ptr<AMXFunction> getPublicFunction(const std::string &name) const = 0;

};

}

#endif