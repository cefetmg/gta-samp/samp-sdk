#ifndef AMX_INDEXED_FUNCTION_HPP
#define AMX_INDEXED_FUNCTION_HPP

#include "samp-sdk/function/amx-function.hpp"
#include <string>
#include <list>

namespace sampsdk {

class AMXIndexedFunction : public AMXFunction {

    private:

        AMX *m_amxContext;

        const int m_functionIndex;

        const std::string m_functionName;

    public:

        AMXIndexedFunction(AMX *t_amxContext, int t_functionIndex, std::string t_functionName);

        ~AMXIndexedFunction();

        cell operator()(const std::list<cell> params) const override;

};

}

#endif