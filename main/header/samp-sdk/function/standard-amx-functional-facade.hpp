#ifndef STANDARD_AMX_FUNCTIONAL_FACADE_HPP
#define STANDARD_AMX_FUNCTIONAL_FACADE_HPP

#include "samp-sdk/function/amx-functional-facade.hpp"
#include "samp-sdk/function/amx-function.hpp"
#include <memory>
#include <string>

namespace sampsdk {

class StandardAMXFunctionalFacade : public AMXFunctionalFacade {

private:

    AMX *m_amxContext;

public:

    StandardAMXFunctionalFacade(AMX *t_amxContext);

    ~StandardAMXFunctionalFacade() override;

    inline std::shared_ptr<AMXFunction> getNativeFunction(const std::string &name) const override;

    inline std::shared_ptr<AMXFunction> getPublicFunction(const std::string &name) const override;
};

}

#endif