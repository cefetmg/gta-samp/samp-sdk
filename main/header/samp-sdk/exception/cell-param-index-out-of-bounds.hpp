#ifndef CELL_PARAM_INDEX_OUT_OF_BOUNDS_HPP
#define CELL_PARAM_INDEX_OUT_OF_BOUNDS_HPP

#include "samp-sdk/amx/amx.h"
#include <stdexcept>
#include <string>

namespace sampsdk {

    class CellParamIndexOutOfBounds : public std::out_of_range {

        private:

            const AMX *m_amxContext;
            
            const cell *m_cellParams;
            
            const int m_paramsCount;
            
            const int m_paramIndex;

        public:

            CellParamIndexOutOfBounds(AMX *t_amxContext, const cell *t_cellParams, int t_paramsCount, int t_paramIndex);

            virtual ~CellParamIndexOutOfBounds();

            const AMX * amxContext() const;

            const cell * cellParams() const;

            int paramsCount() const;

            int paramIndex() const;

        private:

            virtual const std::string buildErrorMessage(const cell *t_cellParams, int t_paramsCount, int t_paramIndex) const;

    };

}


#endif