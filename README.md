# SA-MP plugin SDK

A SDK/Framework to develop plug-ins for SA-MP Multiplayer servers.

## Building

For release builds:

```sh
$ mkdir build && cd build
$ cmake ..
$ cmake --build .
```

For Windows, add `-A Win32` to the code generation.

## Testing

To test the project, run:

```sh
$ mkdir build && cd build
$ export BUILD_TESTING=1
$ cmake ..
$ cmake --build .
$ ctest . 
```
