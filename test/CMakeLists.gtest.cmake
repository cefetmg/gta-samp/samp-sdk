cmake_minimum_required(VERSION 2.8.2)

project(googletest NONE)

include(ExternalProject)
ExternalProject_Add(
  googletest
  GIT_REPOSITORY https://github.com/google/googletest.git
  GIT_TAG release-1.10.0
  SOURCE_DIR "${CMAKE_SOURCE_DIR}/../extern/googletest"
  BINARY_DIR "${CMAKE_BINARY_DIR}/extern/build/googletest"
  CONFIGURE_COMMAND ""
  BUILD_COMMAND ""
  INSTALL_COMMAND ""
  TEST_COMMAND ""
  CMAKE_ARGS "")
