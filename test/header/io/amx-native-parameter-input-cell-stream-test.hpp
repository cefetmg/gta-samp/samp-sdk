#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "samp-sdk/io/amx-native-parameter-input-stream.hpp"
#include "samp-sdk/io/cell-input-stream-enhanced-operators.template.hpp"
#include "io/params/cell-conversion.hpp"
#include <memory>
#include <assert.h>
#include <vector>
#include <stdexcept>
#include <iostream>

using namespace sampsdk;

class AMXNativeParameterInputStreamTest : public ::testing::Test {

    private:

        AMX m_amx;

        cell *m_input;

    protected:

        AMXNativeParameterInputStreamTest() { }

        ~AMXNativeParameterInputStreamTest() { }

        void SetUp() override { m_input = input(); }

        void TearDown() override { delete[] m_input; }

        std::shared_ptr<CellInputStream> amxNativeParameterInputStreamForCellPointer() {
            return std::shared_ptr<CellInputStream>(new AMXNativeParameterInputStream(&m_amx, m_input));
        }

        std::vector<int> expected() const {
            return { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
        }

    private:

        cell * input() const {
            std::vector<cell> inputValues = { 10 * sizeof(cell), 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            return do_conversion(inputValues);
        }


};

TEST_F(AMXNativeParameterInputStreamTest, ShouldThrowOutOfBoundsGivenReadBeyondParameter) {
    std::vector<int> expectedValues = expected();
    std::shared_ptr<CellInputStream> cellInputStream = amxNativeParameterInputStreamForCellPointer();
    std::vector<int> actualValues((int) (expectedValues.size()));
    int outOfRangeValue;

    
    for (int i = 0; i < (int) expectedValues.size(); i++) {
        ASSERT_NO_THROW(cellInputStream >> actualValues[i]);
    }

    ASSERT_THAT(actualValues, ::testing::ElementsAreArray(expectedValues));
    ASSERT_THROW(cellInputStream >> outOfRangeValue, std::out_of_range);
}