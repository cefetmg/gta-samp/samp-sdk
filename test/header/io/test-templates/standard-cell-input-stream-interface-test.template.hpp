#ifndef STANDARD_CELL_INPUT_STREAM_INTERFACE_TEST_HPP
#define STANDARD_CELL_INPUT_STREAM_INTERFACE_TEST_HPP

#include "samp-sdk/io/cell-input-stream.hpp"
#include "samp-sdk/io/standard-amx-cell-input-stream.hpp"
#include "io/cell-input-stream-interface-test.hpp"
#include <memory>

using namespace sampsdk;

template <>
cell * CellInputStreamInterfaceTest<StandardAMXCellInputStream>::calculateInputCell() {
    return new cell[5]{
        do_conversion(expectedInt()), 
        do_conversion(expectedFloat()), 
        do_conversion(expectedChar()), 
        *this->createCellString(), 
        *expectedCellReference()
    };
}

template <>
cell * CellInputStreamInterfaceTest<StandardAMXCellInputStream>::stringAddress() {
    return (m_inputCell + 3);
}

template <>
std::shared_ptr<CellInputStream> CellInputStreamInterfaceTest<StandardAMXCellInputStream>::inputStream() {
    return std::shared_ptr<CellInputStream>(new StandardAMXCellInputStream(&m_amx, cellInput()));
}

template <>
std::shared_ptr<CellInputStream> CellInputStreamInterfaceTest<StandardAMXCellInputStream>::inputStreamWithNullCell() {
    return std::shared_ptr<CellInputStream>(new StandardAMXCellInputStream(&m_amx, nullptr));
}

template <>
std::shared_ptr<CellInputStream> CellInputStreamInterfaceTest<StandardAMXCellInputStream>::inputStreamWithNullAMX() {
    return std::shared_ptr<CellInputStream>(new StandardAMXCellInputStream(nullptr, cellInput()));
}

#endif