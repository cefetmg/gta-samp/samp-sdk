#ifndef AMX_NATIVE_PARAMETER_INPUT_STREAM_INTERFACE_TEST_HPP
#define AMX_NATIVE_PARAMETER_INPUT_STREAM_INTERFACE_TEST_HPP

#include "samp-sdk/io/cell-input-stream.hpp"
#include "samp-sdk/io/standard-amx-cell-input-stream.hpp"
#include "io/cell-input-stream-interface-test.hpp"
#include <memory>

using namespace sampsdk;

template <>
cell * CellInputStreamInterfaceTest<AMXNativeParameterInputStream>::calculateInputCell() {
    return new cell[6] {
        (cell) 6*sizeof(cell),
        do_conversion(expectedInt()), 
        do_conversion(expectedFloat()), 
        do_conversion(expectedChar()), 
        *this->createCellString(), 
        *expectedCellReference()
    };
}

template <>
cell * CellInputStreamInterfaceTest<AMXNativeParameterInputStream>::stringAddress() {
    return (m_inputCell + 4);
}

template <>
std::shared_ptr<CellInputStream> CellInputStreamInterfaceTest<AMXNativeParameterInputStream>::inputStream() {
    return std::shared_ptr<CellInputStream>(new AMXNativeParameterInputStream(&m_amx, cellInput()));
}

template <>
std::shared_ptr<CellInputStream> CellInputStreamInterfaceTest<AMXNativeParameterInputStream>::inputStreamWithNullCell() {
    return std::shared_ptr<CellInputStream>(new AMXNativeParameterInputStream(&m_amx, nullptr));
}

template <>
std::shared_ptr<CellInputStream> CellInputStreamInterfaceTest<AMXNativeParameterInputStream>::inputStreamWithNullAMX() {
    return std::shared_ptr<CellInputStream>(new AMXNativeParameterInputStream(nullptr, cellInput()));
}

#endif