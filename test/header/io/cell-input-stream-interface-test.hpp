
#ifndef CELL_INPUT_STREAM_INTERFACE_TEST_HPP
#define CELL_INPUT_STREAM_INTERFACE_TEST_HPP

#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "samp-sdk/amx/amx.h"
#include "mock/amx/amx-mock.hpp"
#include "mock/amx/amx-mock-factory.hpp"
#include "samp-sdk/io/cell-input-stream.hpp"
#include "samp-sdk/io/cell-input-stream-enhanced-operators.template.hpp"
#include "samp-sdk/io/standard-amx-cell-input-stream.hpp"
#include "samp-sdk/io/amx-native-parameter-input-stream.hpp"
#include "io/params/cell-conversion.hpp"
#include "mock/amx/amx-get-addr-mock-handler.hpp"
#include "mock/amx/amx-str-len-mock-handler.hpp"
#include "mock/amx/amx-get-string-mock-handler.hpp"
#include <memory>
#include <string>
#include <stdexcept>

using namespace amxmock;
using namespace sampsdk;

template <typename T>
class CellInputStreamInterfaceTest : public ::testing::Test {

    private:

        AMX m_amx;

        AMXMock &m_amxMock = get_instance();

        cell *m_inputCell;

        const int m_expectedInt;

        const float m_expectedFloat;

        const char m_expectedChar;

        const std::string m_expectedString;

        const std::unique_ptr<cell> m_expectedCellReference;

    protected:

        CellInputStreamInterfaceTest(): 
            m_expectedInt(INT_MAX), 
            m_expectedFloat(-22.000000001), 
            m_expectedChar('a'), 
            m_expectedString("One Piece is the best!"), 
            m_expectedCellReference(std::unique_ptr<cell>(new cell(1))) 
        {
            m_inputCell = this->calculateInputCell();
        }

        ~CellInputStreamInterfaceTest() override { delete[] m_inputCell; }

        void SetUp() override { }

        void TearDown() override {
            Mock::VerifyAndClear(&m_amxMock);
        }

        std::shared_ptr<CellInputStream> inputStream();

        std::shared_ptr<CellInputStream> inputStreamWithNullCell();

        std::shared_ptr<CellInputStream> inputStreamWithNullAMX();

        int expectedInt() { return m_expectedInt; }

        float expectedFloat() { return m_expectedFloat; }

        char expectedChar() { return m_expectedChar; }

        std::string expectedString() { return m_expectedString; }

        const cell * expectedCellReference() { return m_expectedCellReference.get(); }

        cell * cellInput() { return m_inputCell; }

        void prepareStringMocks() {
            cell *mockAddress = new cell();
            EXPECT_CALL(this->m_amxMock, amx_GetAddr(_, *this->stringAddress(), _))
                .WillOnce(::testing::Invoke(AMXGetAddrMockHandler(&m_amx, *this->stringAddress(), mockAddress)));
            EXPECT_CALL(this->m_amxMock, amx_StrLen(_, _))
                .WillOnce(::testing::Invoke(AMXStrLenMockHandler(mockAddress, this->expectedString().size())));
            EXPECT_CALL(this->m_amxMock, amx_GetString(_, _, _, _))
                .WillOnce(::testing::Invoke(AMXGetStringMockHandler(this->expectedString(), mockAddress, (size_t) this->expectedString().size() + 1)));
            delete mockAddress;
        }

        void prepareCellReferenceMock() {
            EXPECT_CALL(this->m_amxMock, amx_GetAddr(_, *this->expectedCellReference(), _))
                .WillOnce(::testing::Invoke(AMXGetAddrMockHandler(&m_amx, *this->expectedCellReference(), (cell *) this->expectedCellReference())));
        }


    private:

        cell * calculateInputCell();

        cell * stringAddress();

        std::shared_ptr<cell> createCellString() {
            return std::shared_ptr<cell>(do_conversion(expectedString()), [](cell *t_stringAddress) -> void { delete[] t_stringAddress; });
        }
};

#include "test-templates/cell-input-stream-interface-test.template.hpp"

TYPED_TEST_SUITE_P(CellInputStreamInterfaceTest);

TYPED_TEST_P(CellInputStreamInterfaceTest, ReadAllDesiredValuesFromInputStream) {
    int actualInt;
    float actualFloat;
    char actualChar;
    std::string actualString;
    cell *actualCellReference;
    std::shared_ptr<CellInputStream> cellInputStream = this->inputStream();
    this->prepareStringMocks();
    this->prepareCellReferenceMock();

    cellInputStream >> actualInt >> actualFloat >> actualChar >> actualString >> actualCellReference;

    ASSERT_THAT(actualInt, Eq(this->expectedInt()));
    ASSERT_THAT(actualFloat, Eq(this->expectedFloat()));
    ASSERT_THAT(actualChar, Eq(this->expectedChar()));
    ASSERT_THAT(actualString, Eq(this->expectedString()));
    ASSERT_THAT(*actualCellReference, Eq(*this->expectedCellReference()));
}

TYPED_TEST_P(CellInputStreamInterfaceTest, ShouldThrowInvalidArgumentGivenNullCellInput) {
    ASSERT_THROW(this->inputStreamWithNullCell(), std::invalid_argument);
}

TYPED_TEST_P(CellInputStreamInterfaceTest, ShouldThrowInvalidArgumentGivenNullAMXInput) {
    ASSERT_THROW(this->inputStreamWithNullAMX(), std::invalid_argument);
}

REGISTER_TYPED_TEST_SUITE_P(CellInputStreamInterfaceTest, 
    ReadAllDesiredValuesFromInputStream, 
    ShouldThrowInvalidArgumentGivenNullCellInput,
    ShouldThrowInvalidArgumentGivenNullAMXInput);

using TestingTypes = ::testing::Types<StandardAMXCellInputStream, AMXNativeParameterInputStream>;
INSTANTIATE_TYPED_TEST_SUITE_P(CellInputStreamInterfaceTestGroup, CellInputStreamInterfaceTest, TestingTypes);

#endif
