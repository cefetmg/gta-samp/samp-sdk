#ifndef CELL_INPUT_STREAM_ARRAY_TEST_PARAMETER_HPP
#define CELL_INPUT_STREAM_ARRAY_TEST_PARAMETER_HPP

#include "samp-sdk/amx/amx.h"
#include "cell-conversion.hpp"
#include <memory>
#include <vector>

template <typename T>
class CellInputStreamArrayTestParameter  {

    private:

        std::vector<T> m_expectedValues;
        
        std::shared_ptr<cell> m_cellArrayPointer;
        
        std::shared_ptr<cell> m_inputCell;

    public:

        CellInputStreamArrayTestParameter(vector<T> t_expectedValues, cell t_inputCell): 
            m_expectedValues(t_expectedValues), m_inputCell(new cell(t_inputCell)) {

            m_cellArrayPointer = std::shared_ptr<cell>(do_conversion(m_expectedValues), [](cell *t_cellArray) { delete[] t_cellArray; } );
        }

        std::vector<T> expectedValues() const { return m_expectedValues; }

        cell * inputCell() const { return m_inputCell.get(); }

        cell * arrayAddress() const { return m_cellArrayPointer.get(); }

};


template <typename T>
std::string vectorToString(const std::vector<T> expectedValues) {
    std::string result = "vector{";

    if (!expectedValues.empty()) {
        int i = 0;
        result += (std::to_string(expectedValues[i]));

        for (; i < (int) expectedValues.size(); i++) {
            result += (", ");
            result += (std::to_string(expectedValues[i]));
        }
    }

    result.push_back('}');
    return result;
}

template <typename T>
std::ostream& operator << (std::ostream& os, const CellInputStreamArrayTestParameter<T> &testParameter) {
    return os << "CellInputStreamArrayTestParameter{expectedValues=" << vectorToString(testParameter.expectedValues()) << 
        ",arrayAddress="  << testParameter.arrayAddress() << 
        ",inputCell=" << testParameter.inputCell() << '}';
}


#endif