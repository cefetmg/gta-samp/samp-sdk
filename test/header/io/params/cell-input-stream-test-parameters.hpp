#ifndef CELL_INPUT_STREAM_TEST_PARAMETERS_HPP
#define CELL_INPUT_STREAM_TEST_PARAMETERS_HPP

#include "samp-sdk/amx/amx.h"
#include "cell-conversion.hpp"
#include <iostream>
#include <string>

template <typename T>
class CellInputStreamTestParameter {
    
    protected:

        T m_expectedValue;
        
        shared_ptr<cell> m_inputCell;

    protected:

        CellInputStreamTestParameter(T t_expectedValue, cell *t_inputCell): m_expectedValue(t_expectedValue), m_inputCell(shared_ptr<cell>(t_inputCell)) {}

    public:

        CellInputStreamTestParameter(T t_expectedValue) : CellInputStreamTestParameter(t_expectedValue, new cell(do_conversion(t_expectedValue))) {}

        T expectedValue() const { return m_expectedValue; }

        cell * inputCell() const { return m_inputCell.get(); }

};


template <typename T>
std::ostream& operator << (std::ostream& os, const CellInputStreamTestParameter<T> &testParameter) {
    return os << "CellInputStreamTestParameter{expectedValue=" << testParameter.expectedValue() << 
        ",inputCell=" << testParameter.inputCell() << '}';
}

#endif