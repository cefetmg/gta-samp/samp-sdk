#ifndef CELL_CONVERSION_HPP
#define CELL_CONVERSION_HPP

#include "samp-sdk/amx/amx.h"
#include <vector>
#include <string>

template <typename T>
cell do_conversion(T value) {
    return (cell) value;
}

template <>
cell do_conversion<float>(float value) {
   return amx_ftoc(value);
}

cell * do_conversion(std::string value) {
    const int stringSize = (int) value.size();
    cell *cellArray = new cell[stringSize];
    transform(value.begin(), value.end(), cellArray, [](char charValue) -> cell { return do_conversion(charValue); });
    return cellArray;
}

template <typename T>
cell * do_conversion(std::vector<T> values) {
    const int numberOfElements = values.size();
    cell *cellArray = new cell[numberOfElements];
    transform(values.begin(), values.end(), cellArray, [](T value) -> cell { return do_conversion(value); });
    return cellArray;
}

#endif