#ifndef CELL_INPUT_STREAM_STRING_TEST_PARAMETER_HPP
#define CELL_INPUT_STREAM_STRING_TEST_PARAMETER_HPP

#include "samp-sdk/amx/amx.h"
#include "cell-conversion.hpp"
#include "cell-input-stream-test-parameters.hpp"
#include <string>
#include <memory>

class CellInputStreamStringTestParameter : public CellInputStreamTestParameter<std::string> {

    protected:

        std::shared_ptr<cell> m_stringAddress;

    public:

        CellInputStreamStringTestParameter(std::string t_expectedValue, cell t_inputCell): CellInputStreamTestParameter(t_expectedValue, new cell(t_inputCell)) {
            m_stringAddress = std::shared_ptr<cell>(do_conversion(t_expectedValue), [](cell *t_value) -> void { delete[] t_value; });
        }

        cell * stringAddress() const { return m_stringAddress.get(); }

};

std::ostream& operator << (std::ostream& os, const CellInputStreamStringTestParameter &testParameter) {
    return os << "CellInputStreamArrayStringTestParameter{expectedValue=" << testParameter.expectedValue() <<
         ",arrayAddress=" << testParameter.stringAddress() << 
         ",inputCell=" << testParameter.inputCell() << '}';
}


#endif