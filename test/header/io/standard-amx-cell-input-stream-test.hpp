#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "params/cell-input-stream-test-parameters.hpp"
#include "params/cell-input-stream-string-test-parameter.hpp"
#include "params/cell-input-stream-array-test-parameter.hpp"
#include "samp-sdk/io/standard-amx-cell-input-stream.hpp"
#include "samp-sdk/io/cell-input-stream-enhanced-operators.template.hpp"
#include "mock/amx/amx-mock-factory.hpp"
#include "mock/amx/amx-get-addr-mock-handler.hpp"
#include "mock/amx/amx-str-len-mock-handler.hpp"
#include "mock/amx/amx-get-string-mock-handler.hpp"
#include <vector>
#include <string>
#include <string.h>
#include <memory>
#include <limits.h>

using namespace ::testing;
using namespace sampsdk;
using namespace std;
using namespace amxmock;

class StandardAMXCellInputStreamTest : public Test {

    private:

        AMX m_amx;

        AMXMock &m_amxMock = amxmock::get_instance();

    protected:

        StandardAMXCellInputStreamTest() { }

        ~StandardAMXCellInputStreamTest() { }

        void SetUp() override { }

        void TearDown() override {
            Mock::VerifyAndClear(&m_amxMock);
        }

        shared_ptr<CellInputStream> getStandardAmxCellInputStreamForCellPointer(cell *t_cellPointer) {
            return shared_ptr<CellInputStream>(new StandardAMXCellInputStream(&m_amx, t_cellPointer));
        }

        void prepareCellArrayMock(cell t_expectedDelegateContext, cell *t_expectedArrayAddress) {
            EXPECT_CALL(m_amxMock, amx_GetAddr(_, _, _))
                .WillOnce(Invoke(AMXGetAddrMockHandler(&m_amx, t_expectedDelegateContext, t_expectedArrayAddress)));
        }

        void prepareCellStringMock(cell t_expectedCellWithAddress, cell *t_expectedStringAddress, string t_expectedString) {
            EXPECT_CALL(m_amxMock, amx_GetAddr(_, _, _))
                .WillOnce(Invoke(AMXGetAddrMockHandler(&m_amx, t_expectedCellWithAddress, t_expectedStringAddress)));
            EXPECT_CALL(m_amxMock, amx_StrLen(_, _))
                .WillOnce(Invoke(AMXStrLenMockHandler(t_expectedStringAddress, (int) t_expectedString.size())));
            EXPECT_CALL(m_amxMock, amx_GetString(_, _, _, _))
                .WillOnce(Invoke(AMXGetStringMockHandler(t_expectedString, t_expectedStringAddress, t_expectedString.size() + 1)));
        }

};

class StandardAMXCellInputStreamIntValueTest : public StandardAMXCellInputStreamTest, public WithParamInterface<CellInputStreamTestParameter<int>> { };

class StandardAMXCellInputStreamIntArrayValueTest : public StandardAMXCellInputStreamTest, public WithParamInterface<CellInputStreamArrayTestParameter<int>> { };

class StandardAMXCellInputStreamCharValueTest : public StandardAMXCellInputStreamTest, public WithParamInterface<CellInputStreamTestParameter<char>> { };

class StandardAMXCellInputStreamCharArrayValueTest : public StandardAMXCellInputStreamTest, public WithParamInterface<CellInputStreamArrayTestParameter<char>> { };

class StandardAMXCellInputStreamFloatValueTest : public StandardAMXCellInputStreamTest, public WithParamInterface<CellInputStreamTestParameter<float>> { };

class StandardAMXCellInputStreamFloatArrayValueTest : public StandardAMXCellInputStreamTest, public WithParamInterface<CellInputStreamArrayTestParameter<float>> { };

class StandardAMXCellInputStreamStringValueTest : public StandardAMXCellInputStreamTest, public WithParamInterface<CellInputStreamStringTestParameter> { };

TEST_P(StandardAMXCellInputStreamIntValueTest, ShouldReadIntValueFromCell) {
    CellInputStreamTestParameter<int> testParameter = GetParam();
    int actualValue;
    shared_ptr<CellInputStream> cellInputStream = getStandardAmxCellInputStreamForCellPointer(testParameter.inputCell());
    
    cellInputStream >> actualValue;

    ASSERT_THAT(actualValue, Eq(testParameter.expectedValue()));
}

TEST_P(StandardAMXCellInputStreamIntArrayValueTest, ShouldReadIntArrayFromCellArray) {
    CellInputStreamArrayTestParameter<int> testParameter = GetParam();
    prepareCellArrayMock(*testParameter.inputCell(), testParameter.arrayAddress());
    vector<int> actualValues(testParameter.expectedValues().size());
    shared_ptr<CellInputStream> cellInputStream  = getStandardAmxCellInputStreamForCellPointer(testParameter.inputCell());
    cell *actualArrayPointer;

    cellInputStream >> actualArrayPointer;
    shared_ptr<CellInputStream> arrayInputStream = getStandardAmxCellInputStreamForCellPointer(actualArrayPointer);
    for (int i = 0; i < (int) testParameter.expectedValues().size(); i++) {
        arrayInputStream >> actualValues[i];
    }

    ASSERT_THAT(actualValues, ElementsAreArray(testParameter.expectedValues()));
}

TEST_P(StandardAMXCellInputStreamCharValueTest, ShouldReadCharValueFromCell) {
    CellInputStreamTestParameter<char> testParameter = GetParam();
    char actualValue;
    shared_ptr<CellInputStream> cellInputStream = getStandardAmxCellInputStreamForCellPointer(testParameter.inputCell());
    
    cellInputStream >> actualValue;

    ASSERT_THAT(actualValue, Eq(testParameter.expectedValue()));
}

TEST_P(StandardAMXCellInputStreamCharArrayValueTest, ShouldReadCharArrayFromCellArray) {
    CellInputStreamArrayTestParameter<char> testParameter = GetParam();
    prepareCellArrayMock(*testParameter.inputCell(), testParameter.arrayAddress());
    vector<char> actualValues(testParameter.expectedValues().size());
    shared_ptr<CellInputStream> cellInputStream  = getStandardAmxCellInputStreamForCellPointer(testParameter.inputCell());
    cell *actualArrayPointer;

    cellInputStream >> actualArrayPointer;
    shared_ptr<CellInputStream> arrayInputStream = getStandardAmxCellInputStreamForCellPointer(actualArrayPointer);
    for (int i = 0; i < (int) testParameter.expectedValues().size(); i++) {
        arrayInputStream >> actualValues[i];
    }

    ASSERT_THAT(actualValues, ElementsAreArray(testParameter.expectedValues()));
}

TEST_P(StandardAMXCellInputStreamFloatValueTest, ShouldReadFloatValueFromCell) {
    CellInputStreamTestParameter<float> testParameter = GetParam();
    float actualValue;
    shared_ptr<CellInputStream> cellInputStream = getStandardAmxCellInputStreamForCellPointer(testParameter.inputCell());
    
    cellInputStream >> actualValue;

    ASSERT_THAT(actualValue, Eq(testParameter.expectedValue()));
}

TEST_P(StandardAMXCellInputStreamFloatArrayValueTest, ShouldReadFloatArrayFromCellArray) {
    CellInputStreamArrayTestParameter<float> testParameter = GetParam();
    prepareCellArrayMock(*testParameter.inputCell(), testParameter.arrayAddress());
    vector<float> actualValues(testParameter.expectedValues().size());
    shared_ptr<CellInputStream> cellInputStream  = getStandardAmxCellInputStreamForCellPointer(testParameter.inputCell());
    cell *actualArrayPointer;

    cellInputStream >> actualArrayPointer;
    shared_ptr<CellInputStream> arrayInputStream = getStandardAmxCellInputStreamForCellPointer(actualArrayPointer);
    for (int i = 0; i < (int) testParameter.expectedValues().size(); i++) {
        arrayInputStream >> actualValues[i];
    }

    ASSERT_THAT(actualValues, ElementsAreArray(testParameter.expectedValues()));
}

TEST_P(StandardAMXCellInputStreamStringValueTest, ShouldReadStringValueFromCell) {
    CellInputStreamStringTestParameter testParameter = GetParam();
    string actualValue;
    prepareCellStringMock(*testParameter.inputCell(), testParameter.stringAddress(), testParameter.expectedValue());
    shared_ptr<CellInputStream> cellInputStream = getStandardAmxCellInputStreamForCellPointer(testParameter.inputCell());
    
    cellInputStream >> actualValue;

    ASSERT_THAT(actualValue, Eq(testParameter.expectedValue()));
}

INSTANTIATE_TEST_SUITE_P(StandardAMXCellInputStreamIntValueTestGroup, StandardAMXCellInputStreamIntValueTest, 
    Values(CellInputStreamTestParameter<int>(23478), 
        CellInputStreamTestParameter<int>(-47), 
        CellInputStreamTestParameter<int>(0), 
        CellInputStreamTestParameter<int>(INT_MAX)));

INSTANTIATE_TEST_SUITE_P(StandardAMXCellInputStreamIntArrayValueTestGroup, StandardAMXCellInputStreamIntArrayValueTest, 
    Values(CellInputStreamArrayTestParameter<int>({23478, -47, 0, INT_MAX}, (cell) 1),
        CellInputStreamArrayTestParameter<int>({}, (cell) 1),
        CellInputStreamArrayTestParameter<int>({1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}, (cell) 1)));

INSTANTIATE_TEST_SUITE_P(StandardAMXCellInputStreamCharValueTestGroup, StandardAMXCellInputStreamCharValueTest, 
    Values(CellInputStreamTestParameter<char>('o'), 
        CellInputStreamTestParameter<char>('n'), 
        CellInputStreamTestParameter<char>('3'), 
        CellInputStreamTestParameter<char>(' '), 
        CellInputStreamTestParameter<char>('p'), 
        CellInputStreamTestParameter<char>('e'), 
        CellInputStreamTestParameter<char>('c'), 
        CellInputStreamTestParameter<char>('e'), 
        CellInputStreamTestParameter<char>('!'),
        CellInputStreamTestParameter<char>('i'),
        CellInputStreamTestParameter<char>('s'),
        CellInputStreamTestParameter<char>('\t'),
        CellInputStreamTestParameter<char>('g'),
        CellInputStreamTestParameter<char>('r'),
        CellInputStreamTestParameter<char>('e'),
        CellInputStreamTestParameter<char>('@'),
        CellInputStreamTestParameter<char>('t'),
        CellInputStreamTestParameter<char>('\\'),
        CellInputStreamTestParameter<char>('\n'),
        CellInputStreamTestParameter<char>(EOF), 
        CellInputStreamTestParameter<char>('\0')));

INSTANTIATE_TEST_SUITE_P(StandardAMXCellInputStreamCharArrayValueTestGroup, StandardAMXCellInputStreamCharArrayValueTest, 
    Values(CellInputStreamArrayTestParameter<char>({'o', 'n', 'e', ' ', 'p', 'i', 'e', 'c', 'e', '!', 'i', 's', '\t', 'g', 'r', 'e', '@', 't', '\\', '\n', EOF, '\0'}, (cell) 1),
        CellInputStreamArrayTestParameter<char>({}, (cell) 1)));

INSTANTIATE_TEST_SUITE_P(StandardAMXCellInputStreamFloatValueTestGroup, StandardAMXCellInputStreamFloatValueTest, 
    Values(CellInputStreamTestParameter<float>(46.32578), 
        CellInputStreamTestParameter<float>(-47.0000), 
        CellInputStreamTestParameter<float>(0.0), 
        CellInputStreamTestParameter<float>(0.0000001), 
        CellInputStreamTestParameter<float>(FLT_MAX), 
        CellInputStreamTestParameter<float>(FLT_MIN)));

INSTANTIATE_TEST_SUITE_P(StandardAMXCellInputStreamFloatArrayValueTestGroup, StandardAMXCellInputStreamFloatArrayValueTest, 
    Values(CellInputStreamArrayTestParameter<float>({23478.0001, -47E-2, 0.0000000, FLT_MIN, FLT_MAX}, (cell) 1),
        CellInputStreamArrayTestParameter<float>({}, (cell) 1),
        CellInputStreamArrayTestParameter<float>({.1, .1, .1, .1, .1, .1, .1, 1, .1, 1, .1, 1, 1, 1, .1, 1, 1, .1, .1, 1, .1, 1, .1, 1, .1, 1, .1, 1, 1, 1}, (cell) 1)));

INSTANTIATE_TEST_SUITE_P(StandardAMXCellInputStreamStringValueTestGroup, StandardAMXCellInputStreamStringValueTest, 
    Values(CellInputStreamStringTestParameter("This is just a dummy string", (cell) 1),
        CellInputStreamStringTestParameter("Another string just\tto test special\ncaracters just like th3se 0n3s!$$", (cell) 1),
        CellInputStreamStringTestParameter("One Piece is the best anime/mangá in the world!!!!", (cell) 1)));