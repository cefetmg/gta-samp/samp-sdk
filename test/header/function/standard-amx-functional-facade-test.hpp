#include "gtest/gtest.h"
#include "samp-sdk/function/standard-amx-functional-facade.hpp"
#include "mock/amx/amx-mock-factory.hpp"
#include <memory>

using namespace ::testing;
using namespace sampsdk;
using namespace std;
using namespace amxmock;

class AMXFunctionalFacadeTest : public Test {

protected:

    AMXMock &amxMock = amxmock::get_instance();

    AMXFunctionalFacade *facade;

protected:

    AMXFunctionalFacadeTest() { }

    ~AMXFunctionalFacadeTest() { }

    void SetUp() override {
        facade = new StandardAMXFunctionalFacade(nullptr);
    }

    void TearDown() override {
        Mock::VerifyAndClear(&amxMock);
        delete facade;
    }

};

TEST_F(AMXFunctionalFacadeTest, ShouldCallFindNativeFunctionFromSDK) {
    EXPECT_CALL(amxMock, amx_FindNative(_, StrEq("DummyNativeName"), _))
        .WillOnce(Return(AMX_ERR_NONE));

    shared_ptr<AMXFunction> result = facade->getNativeFunction("DummyNativeName");

    ASSERT_NE(nullptr, result.get());
}