#ifndef AMX_MOCK_FACTORY_HPP
#define AMX_MOCK_FACTORY_HPP

#include "mock/amx/amx-mock.hpp"

namespace amxmock {

AMXMock & get_instance();

}

#endif