#ifndef AMX_GET_ADDR_MOCK_HANDLER_HPP
#define AMX_GET_ADDR_MOCK_HANDLER_HPP

#include "mock/amx/amx-api-mock-handler.hpp"
#include "samp-sdk/amx/amx.h"

namespace amxmock {

    class AMXGetAddrMockHandler: public AMXApiMockHandler {

        private:

            AMX *m_amxContext;

            cell m_expectedAddress;
            
            cell *m_physicalAddress;

        public:

            AMXGetAddrMockHandler(AMX *t_amxContext, cell t_expectedAddress, cell *t_physicalAddress);

            AMXGetAddrMockHandler(AMX *t_amxContext, cell t_expectedAddress, cell *t_physicalAddress, int m_statusCode);

            int AMXAPI operator () (AMX *t_amxContext, cell t_expectedAddress, cell **t_physicalAddress) const;

    };

}

#endif