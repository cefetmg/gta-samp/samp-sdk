#ifndef AMX_GET_STRING_MOCK_HANDLER_HPP
#define AMX_GET_STRING_MOCK_HANDLER_HPP

#include "samp-sdk/amx/amx.h"
#include "mock/amx/amx-api-mock-handler.hpp"
#include <string>

namespace amxmock {

    class AMXGetStringMockHandler : public AMXApiMockHandler {

        private:

            const std::string m_expectedString;

            const cell *m_stringAddress;

            const int m_expectedUseWchar;

            const size_t m_stringSize;

        public:

            AMXGetStringMockHandler(const std::string t_expectedString, const cell *t_stringAddress, const size_t t_stringSize);

            AMXGetStringMockHandler(const std::string t_expectedString, const cell *t_stringAddress, const size_t t_stringSize, int t_statusCode);

            int AMXAPI operator () (char *t_destinationString, const cell *t_stringAddress, int t_userWchar, size_t t_stringSize) const;

    };

}

#endif