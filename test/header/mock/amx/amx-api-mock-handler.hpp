#ifndef AMX_API_MOCK_HANDLER_HPP
#define AMX_API_MOCK_HANDLER_HPP

namespace amxmock {

    class AMXApiMockHandler {
        
        protected:

            int m_statusCode;

        protected:

            AMXApiMockHandler();

            AMXApiMockHandler(int t_statusCode);

        public:

            virtual ~AMXApiMockHandler() {}

            virtual int statusCode() const;

    };
}

#endif