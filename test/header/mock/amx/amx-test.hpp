#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "mock/amx/amx-mock-factory.hpp"
#include "mock/amx/amx-mock.hpp"
#include "samp-sdk/amx/amx.h"
#include <string.h>
#include <string>

using namespace std;
using namespace ::testing;
using namespace amxmock;

class AMXMockTest : public Test {

protected:

    AMXMock &amxMock = get_instance();

protected:

    AMXMockTest() {}

    ~AMXMockTest() {}

    void SetUp() override {}

    void TearDown() override {
        Mock::VerifyAndClear(&amxMock);
    }

};

TEST_F(AMXMockTest, ShouldCallAlign16FromServerWithExpectedMemoryAddress) {
    uint16_t value = 160;
    EXPECT_CALL(amxMock, amx_Align16(Eq(&value)))
        .Times(1);

    amx_Align16(&value);
}

TEST_F(AMXMockTest, ShouldCallAlign32FromServerWithExpectedMemoryAddress) {
    uint32_t value = 160;
    EXPECT_CALL(amxMock, amx_Align32(Eq(&value)))
        .Times(1);

    amx_Align32(&value);
}

TEST_F(AMXMockTest, ShouldCallAllotFromServerWithExpectedParams) {
    AMX amx;
    cell amxAddress;
    cell *cppAddress = new cell();
    EXPECT_CALL(amxMock, amx_Allot(Eq(&amx), Eq(123), Eq(&amxAddress), Eq(&cppAddress)))
        .Times(1);

    amx_Allot(&amx, 123, &amxAddress, &cppAddress);

    delete cppAddress;
}

TEST_F(AMXMockTest, ShouldCallCallbackFromServerWithExpectedParams) {
    AMX amx;
    int callbackIndex = 1;
    cell result;
    cell params[] = {1, 2, 3, 4, 5};
    EXPECT_CALL(amxMock, amx_Callback(Eq(&amx), Eq(1), Eq(&result), Eq(&params[0])))
        .Times(1);

    amx_Callback(&amx, callbackIndex, &result, params);
}

TEST_F(AMXMockTest, ShouldCallCleanupFromServerWithExpectedParams) {
    AMX amx;
    EXPECT_CALL(amxMock, amx_Cleanup(Eq(&amx)))
        .Times(1);

    amx_Cleanup(&amx);
}

TEST_F(AMXMockTest, ShouldCallCloneFromServerWithExpectedParams) {
    AMX amxDest;
    AMX amxSource;
    int rawData = 4;
    void *data = &rawData;
    EXPECT_CALL(amxMock, amx_Clone(Eq(&amxDest), Eq(&amxSource), Eq(&rawData)))
        .Times(1);

    amx_Clone(&amxDest, &amxSource, data);
}

TEST_F(AMXMockTest, ShouldCallExecFromServerWithExpectedParams) {
    AMX amx;
    cell retval;
    int functionIndex = 4;
    EXPECT_CALL(amxMock, amx_Exec(Eq(&amx), Eq(&retval), Eq(4)))
        .Times(1);

    amx_Exec(&amx, &retval, functionIndex);
}

TEST_F(AMXMockTest, ShouldCallFindNativeFromServerWithExpectedParams) {
    AMX amx;
    int functionIndex;
    EXPECT_CALL(amxMock, amx_FindNative(Eq(&amx), StrEq("DummyNative"), Eq(&functionIndex)))
        .Times(1);

    amx_FindNative(&amx, "DummyNative", &functionIndex);
}

TEST_F(AMXMockTest, ShouldCallFindPublicFromServerWithExpectedParams) {
    AMX amx;
    int functionIndex;
    EXPECT_CALL(amxMock, amx_FindPublic(Eq(&amx), StrEq("DummyPublic"), Eq(&functionIndex)))
        .Times(1);

    amx_FindPublic(&amx, "DummyPublic", &functionIndex);
}

TEST_F(AMXMockTest, ShouldCallFindPubVarFromServerWithExpectedParams) {
    AMX amx;
    int functionIndex;
    EXPECT_CALL(amxMock, amx_FindPubVar(Eq(&amx), StrEq("DummyPubVar"), Eq(&functionIndex)))
        .Times(1);

    amx_FindPubVar(&amx, "DummyPubVar", &functionIndex);
}

TEST_F(AMXMockTest, ShouldCallFindTagIdFromServerWithExpectedParams) {
    AMX amx;
    char *tagName = new char[1]; //use amx_NameLength to get the size
    EXPECT_CALL(amxMock, amx_FindTagId(Eq(&amx), Eq(11), Eq(&tagName[0])))
        .Times(1);

    amx_FindTagId(&amx, 11, tagName); //insert the tag name according to the tag ID
    
    delete[] tagName;
}

TEST_F(AMXMockTest, ShouldCallFindFlagsFromServerWithExpectedParams) {
    AMX amx;
    uint16_t flags;
    EXPECT_CALL(amxMock, amx_Flags(Eq(&amx), Eq(&flags)))
        .Times(1);

    amx_Flags(&amx, &flags);
}

TEST_F(AMXMockTest, ShouldCallGetAddrFromServerWithExpectedParams) {
    AMX amx;
    cell amxAddr = 10;
    cell *cppAddr = new cell();
    EXPECT_CALL(amxMock, amx_GetAddr(Eq(&amx), Eq(10), Eq(&cppAddr)))
        .Times(1);

    amx_GetAddr(&amx, amxAddr, &cppAddr);

    delete cppAddr;
}

TEST_F(AMXMockTest, ShouldCallGetNativeFromServerWithExpectedParams) {
    AMX amx;
    int index = 40;
    char *functionName = new char[1];
    EXPECT_CALL(amxMock, amx_GetNative(Eq(&amx), Eq(40), Eq(&functionName[0])))
        .Times(1);

    amx_GetNative(&amx, index, functionName);

    delete[] functionName;
}

TEST_F(AMXMockTest, ShouldCallGetPublicFromServerWithExpectedParams) {
    AMX amx;
    int index = 40;
    char *functionName = new char[1];
    EXPECT_CALL(amxMock, amx_GetPublic(Eq(&amx), Eq(40), Eq(&functionName[0])))
        .Times(1);

    amx_GetPublic(&amx, index, functionName);

    delete[] functionName;
}

TEST_F(AMXMockTest, ShouldCallGetPubVarFromServerWithExpectedParams) {
    AMX amx;
    int index = 34;
    char *publicVarName = new char[1];
    cell amxAddress;
    EXPECT_CALL(amxMock, amx_GetPubVar(Eq(&amx), Eq(34), Eq(&publicVarName[0]), Eq(&amxAddress)))
        .Times(1);

    amx_GetPubVar(&amx, index, publicVarName, &amxAddress);

    delete[] publicVarName;
}


TEST_F(AMXMockTest, ShouldCallGetStringFromServerWithExpectedParams) {
    char *dest = new char[1];
    cell *cppStringAddr = new cell[1];
    EXPECT_CALL(amxMock, amx_GetString(Eq(&dest[0]), Eq(&cppStringAddr[0]), Eq(1), Eq(2)))
        .Times(1);

    amx_GetString(dest, cppStringAddr, 1, 2);

    delete[] dest;
    delete[] cppStringAddr;
}


TEST_F(AMXMockTest, ShouldCallGetTagFromServerWithExpectedParams) {
    AMX amx;
    int index = 10;
    char *tagName = new char[1];
    cell tagId;
    EXPECT_CALL(amxMock, amx_GetTag(Eq(&amx), Eq(10), Eq(&tagName[0]), Eq(&tagId)))
        .Times(1);

    amx_GetTag(&amx, index, tagName, &tagId);

    delete[] tagName;
}

TEST_F(AMXMockTest, ShouldCallGetUserDataFromServerWithExpectedParams) {
    AMX amx;
    struct DataWrapper {
        string word;
        int dummyValue;
    };
    ON_CALL(amxMock, amx_GetUserData).WillByDefault([](AMX *t_amx, long t_tag, void **t_dataStorage) -> int {
        DataWrapper *myData = new DataWrapper();
        *myData = {"I am a string", 25};
        *t_dataStorage = (void *) myData;
        assert(t_amx != nullptr);
        assert(t_tag == 12);
        return AMX_ERR_NONE;
    });
    void *dataStorage;
    EXPECT_CALL(amxMock, amx_GetUserData(Eq(&amx), Eq(12), Eq(&dataStorage)))
        .Times(1);

    amx_GetUserData(&amx, 12, &dataStorage);

    DataWrapper *result = ((DataWrapper *) (dataStorage)); //another option is capturing the memory address inside a smart pointer
    ASSERT_EQ("I am a string", result->word);
    ASSERT_EQ(25, result->dummyValue);
    delete result;
}

TEST_F(AMXMockTest, ShouldCallInitFromServerWithExpectedParams) {
    AMX amx;
    void *program = nullptr;
    EXPECT_CALL(amxMock, amx_Init(Eq(&amx), Eq(program)))
        .Times(1);

    amx_Init(&amx, program);
}

TEST_F(AMXMockTest, ShouldCallInitJITFromServerWithExpectedParams) {
    AMX amx;
    void *relocTable = nullptr;
    void *program = nullptr;
    EXPECT_CALL(amxMock, amx_InitJIT(Eq(&amx), Eq(relocTable), Eq(program)))
        .Times(1);

    amx_InitJIT(&amx, relocTable, program);
}

TEST_F(AMXMockTest, ShouldCallMemInfoFromServerWithExpectedParams) {
    AMX amx;
    long codeSize;
    long dataSize;
    long stackHeapSize;
    ON_CALL(amxMock, amx_MemInfo).WillByDefault([](AMX *t_amx, long *t_codeSize, long *t_dataSize, long *t_stackHeapSize) {
        *t_codeSize = 20;
        *t_dataSize = 45;
        *t_stackHeapSize = 2048;
        assert(t_amx != nullptr);   
        return AMX_ERR_NONE;
    });
    EXPECT_CALL(amxMock, amx_MemInfo(Eq(&amx), Eq(&codeSize), Eq(&dataSize), Eq(&stackHeapSize)))
        .Times(1);

    amx_MemInfo(&amx, &codeSize, &dataSize, &stackHeapSize);

    ASSERT_EQ(20, codeSize);
    ASSERT_EQ(45, dataSize);
    ASSERT_EQ(2048, stackHeapSize);
}

TEST_F(AMXMockTest, ShouldCallNameLengthFromServerWithExpectedParams) {
    AMX amx;
    int length;
    EXPECT_CALL(amxMock, amx_NameLength(Eq(&amx), Eq(&length)))
        .Times(1);

    amx_NameLength(&amx, &length);
}

TEST_F(AMXMockTest, ShouldCallNativeInfoFromServerWithExpectedParams) {
    auto nativeFunction = [](AMX *t_amx, cell *t_params) -> int { 
        assert(t_amx != nullptr);
        assert(t_params != nullptr);
        return AMX_ERR_NONE; 
    };
    EXPECT_CALL(amxMock, amx_NativeInfo(StrEq("DummyNative"), Eq(nativeFunction)))
        .Times(1);

    amx_NativeInfo("DummyNative", nativeFunction);
}

TEST_F(AMXMockTest, ShouldCallNumNativesFromServerWithExpectedParams) {
    AMX amx;
    int numberOfNatives;
    EXPECT_CALL(amxMock, amx_NumNatives(Eq(&amx), Eq(&numberOfNatives)))
        .Times(1);

    amx_NumNatives(&amx, &numberOfNatives);
}

TEST_F(AMXMockTest, ShouldCallNumPublicsFromServerWithExpectedParams) {
    AMX amx;
    int numberOfPublics;
    EXPECT_CALL(amxMock, amx_NumPublics(Eq(&amx), Eq(&numberOfPublics)))
        .Times(1);

    amx_NumPublics(&amx, &numberOfPublics);
}

TEST_F(AMXMockTest, ShouldCallNumPubVarsFromServerWithExpectedParams) {
    AMX amx;
    int numberOfPublicVars;
    EXPECT_CALL(amxMock, amx_NumPubVars(Eq(&amx), Eq(&numberOfPublicVars)))
        .Times(1);

    amx_NumPubVars(&amx, &numberOfPublicVars);
}

TEST_F(AMXMockTest, ShouldCallNumTagsFromServerWithExpectedParams) {
    AMX amx;
    int numberOfTags;
    EXPECT_CALL(amxMock, amx_NumTags(Eq(&amx), Eq(&numberOfTags)))
        .Times(1);

    amx_NumTags(&amx, &numberOfTags);
}

TEST_F(AMXMockTest, ShouldCallPushFromServerWithExpectedParams) {
    AMX amx;
    cell value = 'o';
    EXPECT_CALL(amxMock, amx_Push(Eq(&amx), Eq('o')))
        .Times(1);

    amx_Push(&amx, value);
}

TEST_F(AMXMockTest, ShouldCallPushArrayFromServerWithExpectedParams) {
    AMX amx;
    const cell array[] = {1, 2, 3};
    cell amxAddress;
    cell *cppAddress = new cell();
    EXPECT_CALL(amxMock, amx_PushArray(Eq(&amx), Eq(&amxAddress), Eq(&cppAddress), Eq(&array[0]), Eq(3)))
        .Times(1);

    amx_PushArray(&amx, &amxAddress, &cppAddress, array, 3); //amxAddress needs to be released later
                                                             // cppAddress can be nullptr in case we have no interest in keeping it
    delete cppAddress;
}

TEST_F(AMXMockTest, ShouldCallPushUnpackedStringFromServerWithExpectedParams) {
    AMX amx;
    const char *text = "I am a very big string that should not be packed. Make sure to use it accordingly.";
    cell amxAddress;
    cell *cppAddress = new cell();
    EXPECT_CALL(amxMock, amx_PushString(Eq(&amx), Eq(&amxAddress), Eq(&cppAddress), Eq(&text[0]), Eq(false), Eq(true)))
        .Times(1);

    amx_PushString(&amx, &amxAddress, &cppAddress, text, false, true); //amxAddress needs to be released later

    delete cppAddress;
}

TEST_F(AMXMockTest, ShouldCallPushPackedStringFromServerWithExpectedParams) {
    AMX amx;
    const char *text = "pack";
    cell amxAddress;
    cell *cppAddress = new cell();
    EXPECT_CALL(amxMock, amx_PushString(Eq(&amx), Eq(&amxAddress), Eq(&cppAddress), Eq(&text[0]), Eq(true), Eq(true)))
        .Times(1);

    amx_PushString(&amx, &amxAddress, &cppAddress, text, true, true); //amxAddress needs to be released later

    delete cppAddress;
}

TEST_F(AMXMockTest, ShouldCallRaiseErrorFromServerWithExpectedParams) {
    AMX amx;
    EXPECT_CALL(amxMock, amx_RaiseError(Eq(&amx), Eq(AMX_ERR_FORMAT)))
        .Times(1);

    amx_RaiseError(&amx, AMX_ERR_FORMAT);
}

TEST_F(AMXMockTest, ShouldCallRegisterFromServerWithExpectedParams) {
    AMX amx;
    AMX_NATIVE_INFO nativeList[] = {
        {"MyNative1", [](AMX *t_amx, cell *t_params) -> int { assert(t_amx != nullptr && t_params != nullptr); return 0; }},
        {"MyNative2", [](AMX *t_amx, cell *t_params) -> int { assert(t_amx != nullptr && t_params != nullptr); return 0; }},
        {"MyNative3", [](AMX *t_amx, cell *t_params) -> int { assert(t_amx != nullptr && t_params != nullptr); return 0; }},
        {nullptr, nullptr}
    };
    EXPECT_CALL(amxMock, amx_Register(Eq(&amx), Eq(&nativeList[0]), Eq(-1)))
        .Times(1);

    amx_Register(&amx, nativeList, -1); //registers the natives list with two nullptr's at the end
}

TEST_F(AMXMockTest, ShouldCallReleaseFromServerWithExpectedParams) {
    AMX amx;
    cell amxAddress;
    // amx_Allot(&amx, 22, &amxAddress, nullptr); //allots 22 cells in AMX
    amxAddress = 34553;
    EXPECT_CALL(amxMock, amx_Release(Eq(&amx), Eq(34553)))
        .Times(1);

    amx_Release(&amx, amxAddress); //frees the 22 contiguous cells pointed by amxAddress
}

TEST_F(AMXMockTest, ShouldCallSetCallbackFromServerWithExpectedParams) {
    AMX amx;
    auto callback = [](AMX *t_amx, int t_callbackIndex, cell *t_result, cell *t_params) -> int { 
        assert(t_amx != nullptr);
        assert(t_callbackIndex != 0);
        assert(t_result != nullptr);
        assert(t_params != nullptr);
        return AMX_ERR_NONE; 
    };
    EXPECT_CALL(amxMock, amx_SetCallback(Eq(&amx), Eq(callback)))
        .Times(1);

    amx_SetCallback(&amx, callback);
}

TEST_F(AMXMockTest, ShouldCallSetDebugHookFromServerWithExpectedParams) {
    AMX amx;
    auto debugHook = [](AMX *t_amx) -> int { assert(t_amx != nullptr); return AMX_ERR_NONE; };
    EXPECT_CALL(amxMock, amx_SetDebugHook(Eq(&amx), Eq(debugHook)))
        .Times(1);

    amx_SetDebugHook(&amx, debugHook); //set debugHook to nullptr in case you want to deactivate it
}

TEST_F(AMXMockTest, ShouldCallSetStringFromServerWithExpectedParams) {
    cell *cppAddress = new cell();
    // amx_GetAddr(amx, params[i], &cppAddress); // get the string address in the OS' memory, not AMX's
    string newStr = "A new string to be replaced at cppAddress";
    EXPECT_CALL(amxMock, amx_SetString(Eq(cppAddress), StrEq("A new string to be replaced at cppAddress"), Eq(0), Eq(0), Eq(newStr.size() * sizeof(char))))
        .Times(1);

    amx_SetString(cppAddress, newStr.c_str(), 0, 0, sizeof(char) * newStr.size());

    delete cppAddress;
}

TEST_F(AMXMockTest, ShouldCallSetUserDataFromServerWithExpectedParams) {
    AMX amx;
    long tag = 111111;
    struct DummyUserData {
        struct SubDummyUserData {
            string dummyString;
        };
        SubDummyUserData subDummyData;
    };
    DummyUserData dummyData = {{"This is a dummy string"}};
    void *rawDummyData = &dummyData;
    EXPECT_CALL(amxMock, amx_SetUserData(Eq(&amx), Eq(111111), Eq(&dummyData)))
        .Times(1);

    amx_SetUserData(&amx, tag, rawDummyData);

}

TEST_F(AMXMockTest, ShouldCallSetStrLenFromServerWithExpectedParams) {
    const cell *cppAddress = new cell(); //read with amx_GetAddr at physical address
    int length;
    EXPECT_CALL(amxMock, amx_StrLen(Eq(cppAddress), Eq(&length)))
        .Times(1);

    amx_StrLen(cppAddress, &length);

    delete cppAddress;
}

TEST_F(AMXMockTest, ShouldGetStringUsingStrParamWithExpectedParams) {
    AMX amx;
    cell params[] = {1, 2, 3, 4, 5};
    char *stringParam = nullptr;
    EXPECT_CALL(amxMock, amx_GetString(_, _, _, _))
        .WillOnce(Invoke([](char *t_stringParam, const int *t_stringAddr, int t_pack, unsigned long t_useWchar) {
            assert(t_useWchar != 0);
            assert(t_pack == 0);
            assert(t_stringAddr != nullptr);
            strcpy(t_stringParam, "I am just a dummy C-String");
            return AMX_ERR_NONE;
        }));
    EXPECT_CALL(amxMock, amx_StrLen(_, _))
        .WillOnce(Invoke([](const cell *t_cString, int *t_length) {
            assert(t_cString != nullptr);
            *t_length = strlen("I am just a dummy C-String");
            return AMX_ERR_NONE;
        }));
    EXPECT_CALL(amxMock, amx_GetAddr(Eq(&amx), _, _)).Times(1);

    amx_StrParam(&amx, params[2], stringParam);

    ASSERT_STREQ(stringParam, "I am just a dummy C-String");
    //if the pointer was allocated using alloca, we don't need to free it!
}

TEST_F(AMXMockTest, ShouldCallUTF8CheckFromServerWithExpectedParams) {
    string utf8String = "I am a ÚTF-8 string for just a façade";
    int length;
    EXPECT_CALL(amxMock, amx_UTF8Check(StrEq("I am a ÚTF-8 string for just a façade"), Eq(&length)))
        .Times(1);

    amx_UTF8Check(utf8String.c_str(), &length);
}

TEST_F(AMXMockTest, ShouldCallUTF8GetFromServerWithExpectedParams) {
    cell *decodedChar = new cell[4]; //can read with amx_UTF8Check to fit inside the cell.
    const char *encodedString = "I am a ÚTF-8 string for just a façade";
    const char *strPointer = encodedString;
    cell *originalDecodedChar = decodedChar;
    EXPECT_CALL(amxMock, amx_UTF8Get(StrEq("I am a ÚTF-8 string for just a façade"), Eq(&strPointer), Eq(originalDecodedChar)))
        .Times(1);

    amx_UTF8Get(strPointer, &strPointer, decodedChar++); // this is an iteration from [strPointer, strPointer + n], where n is n bytes
                                                         // behind the UTF-8 that's read. This char is also recorded in the same position pointed
                                                         // by decodedChar. Notice that strPointer is overwritten inside the function, meaning
                                                         // that we might lose its reference if not used accordingly
    
    *decodedChar = 0; //zero-terminate the UTF-8 cell string

    ASSERT_EQ(decodedChar, originalDecodedChar + 1);

    delete[] originalDecodedChar;
}

TEST_F(AMXMockTest, ShouldCallUTF8LenFromServerWithExpectedParams) {
    int length;
    const cell *cppAddress = new cell(); // can be read with amx_GetAddr
    EXPECT_CALL(amxMock, amx_UTF8Len(Eq(cppAddress), Eq(&length)))
        .Times(1);

    amx_UTF8Len(cppAddress, &length); // reads the UTF-8 decoded string's length into `length`

    delete cppAddress;
}

TEST_F(AMXMockTest, ShouldCallUTF8PutFromServerWithExpectedParams) {
    char *encodedStr = new char[4 + 3 + 1]; // reads the UTF-8 decoded string's length from amx_UTF8Len 
                                            // it adds 3 bytes for byte order and one for \0.
    cell *utf8Char = new cell[4]; //this is a pointer to characters of an UTF-8 string, probably decoded using amx_UTF8Get.
                                  //iterate over the cells to get it; it is safe, since it should be zero-terminated.
    char *result = encodedStr; // final result will be stored here
    EXPECT_CALL(amxMock, amx_UTF8Put(Eq(result + 3), _, Eq(1), Eq(1)))
        .Times(1);

    // Insert byte order mark: https://pt.wikipedia.org/wiki/Marca_de_ordem_de_byte
    result[0]='\xef';
    result[1]='\xbb';
    result[2]='\xbf';
    encodedStr = encodedStr + 3; //jumps the byte order mark
    cell *utf8CharItr = utf8Char;
    *utf8CharItr = 1;
    amx_UTF8Put(encodedStr, &encodedStr, 4 - (encodedStr - result), *utf8CharItr++);   // reads the utf8Char, iterating the pointer until the zero-terminating char,
                                                                                      // such that we put the char into the string using the proper amount of bytes (total length - (current - initial))

    ASSERT_EQ(utf8CharItr, utf8Char + 1); // we should jump the utf8 cell string when finishing the first PUT

    delete[] result;
    delete[] utf8Char;
}