#ifndef AMX_MOCK_HPP
#define AMX_MOCK_HPP

#include "samp-sdk/amx/amx.h"
#include "gmock/gmock.h"

namespace amxmock {

class AMXMock {

    friend AMXMock & get_instance();

private:

    AMXMock();

public:

    MOCK_METHOD(uint16_t * AMXAPI, amx_Align16, (uint16_t *v));

    MOCK_METHOD(uint32_t * AMXAPI, amx_Align32, (uint32_t *v));

    #if defined _I64_MAX || defined HAVE_I64
        MOCK_METHOD(uint64_t * AMXAPI, amx_Align64, (uint64_t *v));
    #endif

    MOCK_METHOD(int AMXAPI, amx_Allot, (AMX *amx, int cells, cell *amx_addr, cell **phys_addr));

    MOCK_METHOD(int AMXAPI, amx_Callback, (AMX *amx, cell index, cell *result, cell *params));

    MOCK_METHOD(int AMXAPI, amx_Cleanup, (AMX *amx));

    MOCK_METHOD(int AMXAPI, amx_Clone, (AMX *amxClone, AMX *amxSource, void *data));

    MOCK_METHOD(int AMXAPI, amx_Exec, (AMX *amx, cell *retval, int index));

    MOCK_METHOD(int AMXAPI, amx_FindNative, (AMX *amx, const char *name, int *index));

    MOCK_METHOD(int AMXAPI, amx_FindPublic, (AMX *amx, const char *funcname, int *index));

    MOCK_METHOD(int AMXAPI, amx_FindPubVar, (AMX *amx, const char *varname, cell *amx_addr));

    MOCK_METHOD(int AMXAPI, amx_FindTagId, (AMX *amx, cell tag_id, char *tagname));

    MOCK_METHOD(int AMXAPI, amx_Flags, (AMX *amx,uint16_t *flags));

    MOCK_METHOD(int AMXAPI, amx_GetAddr, (AMX *amx,cell amx_addr,cell **phys_addr));

    MOCK_METHOD(int AMXAPI, amx_GetNative, (AMX *amx, int index, char *funcname));

    MOCK_METHOD(int AMXAPI, amx_GetPublic, (AMX *amx, int index, char *funcname));

    MOCK_METHOD(int AMXAPI, amx_GetPubVar, (AMX *amx, int index, char *varname, cell *amx_addr));

    MOCK_METHOD(int AMXAPI, amx_GetString, (char *dest,const cell *source, int use_wchar, size_t size));

    MOCK_METHOD(int AMXAPI, amx_GetTag, (AMX *amx, int index, char *tagname, cell *tag_id));

    MOCK_METHOD(int AMXAPI, amx_GetUserData, (AMX *amx, long tag, void **ptr));

    MOCK_METHOD(int AMXAPI, amx_Init, (AMX *amx, void *program));

    MOCK_METHOD(int AMXAPI, amx_InitJIT, (AMX *amx, void *reloc_table, void *native_code));

    MOCK_METHOD(int AMXAPI, amx_MemInfo, (AMX *amx, long *codesize, long *datasize, long *stackheap));

    MOCK_METHOD(int AMXAPI, amx_NameLength, (AMX *amx, int *length));

    MOCK_METHOD(AMX_NATIVE_INFO * AMXAPI, amx_NativeInfo, (const char *name, AMX_NATIVE func));

    MOCK_METHOD(int AMXAPI, amx_NumNatives, (AMX *amx, int *number));

    MOCK_METHOD(int AMXAPI, amx_NumPublics, (AMX *amx, int *number));

    MOCK_METHOD(int AMXAPI, amx_NumPubVars, (AMX *amx, int *number));

    MOCK_METHOD(int AMXAPI, amx_NumTags, (AMX *amx, int *number));

    MOCK_METHOD(int AMXAPI, amx_Push, (AMX *amx, cell value));

    MOCK_METHOD(int AMXAPI, amx_PushArray, (AMX *amx, cell *amx_addr, cell **phys_addr, const cell array[], int numcells));

    MOCK_METHOD(int AMXAPI, amx_PushString, (AMX *amx, cell *amx_addr, cell **phys_addr, const char *string, int pack, int use_wchar));

    MOCK_METHOD(int AMXAPI, amx_RaiseError, (AMX *amx, int error));

    MOCK_METHOD(int AMXAPI, amx_Register, (AMX *amx, const AMX_NATIVE_INFO *nativelist, int number));

    MOCK_METHOD(int AMXAPI, amx_Release, (AMX *amx, cell amx_addr));

    MOCK_METHOD(int AMXAPI, amx_SetCallback, (AMX *amx, AMX_CALLBACK callback));

    MOCK_METHOD(int AMXAPI, amx_SetDebugHook, (AMX *amx, AMX_DEBUG debug));

    MOCK_METHOD(int AMXAPI, amx_SetString, (cell *dest, const char *source, int pack, int use_wchar, size_t size));

    MOCK_METHOD(int AMXAPI, amx_SetUserData, (AMX *amx, long tag, void *ptr));

    MOCK_METHOD(int AMXAPI, amx_StrLen, (const cell *cstring, int *length));

    MOCK_METHOD(int AMXAPI, amx_UTF8Check, (const char *string, int *length));

    MOCK_METHOD(int AMXAPI, amx_UTF8Get, (const char *string, const char **endptr, cell *value));

    MOCK_METHOD(int AMXAPI, amx_UTF8Len, (const cell *cstr, int *length));

    MOCK_METHOD(int AMXAPI, amx_UTF8Put, (char *string, char **endptr, int maxchars, cell value));

};
}
#endif