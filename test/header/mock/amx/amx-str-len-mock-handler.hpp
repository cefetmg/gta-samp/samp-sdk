#ifndef AMX_STR_LEN_MOCK_HANDLER_HPP
#define AMX_STR_LEN_MOCK_HANDLER_HPP

#include "mock/amx/amx-api-mock-handler.hpp"
#include "samp-sdk/amx/amx.h"

namespace amxmock {

    class AMXStrLenMockHandler: public AMXApiMockHandler {

        private:

            const cell *m_stringAddress;
            
            const int m_expectedLength;

        public:

            AMXStrLenMockHandler(const cell *t_stringAddress, const int t_expectedLength);

            AMXStrLenMockHandler(const cell *t_stringAddress, const int t_expectedLength, int t_statusCode);

            int AMXAPI operator () (const cell *t_stringAddress, int *t_targetLength) const;

    };

}

#endif