#ifndef CELL_PARAM_INDEX_OUT_OF_BOUNDS_TEST_PARAM_HPP
#define CELL_PARAM_INDEX_OUT_OF_BOUNDS_TEST_PARAM_HPP

#include "samp-sdk/exception/cell-param-index-out-of-bounds.hpp"
#include <vector>
#include <string>
#include <algorithm>
#include <ostream>
#include <memory>

using namespace sampsdk;

class CellParamIndexOutOfBoundsTestParam {

    private:

        AMX amx;

    public:

        CellParamIndexOutOfBounds actualException;

        std::string expectedExceptionMessage;

    public:

        CellParamIndexOutOfBoundsTestParam(std::vector<cell> t_cellArray, int t_cellIndex, std::string t_expectedExceptionMessage): 
            actualException(supplyException(t_cellArray, t_cellIndex)), expectedExceptionMessage(t_expectedExceptionMessage) {}

    private:

        CellParamIndexOutOfBounds supplyException(std::vector<cell> t_cellArray, int t_cellIndex) {
            std::shared_ptr<cell> cellArray = std::shared_ptr<cell>(new cell[t_cellArray.size()], 
                [](cell *t_arrayPointer) -> void { delete[] t_arrayPointer; });
            std::transform(t_cellArray.begin(), t_cellArray.end(), cellArray.get(), [](cell t_cell) -> cell { return t_cell; });
            return CellParamIndexOutOfBounds(&amx, cellArray.get(), (int) t_cellArray.size(), t_cellIndex);
        }


};

std::ostream& operator << (std::ostream& os, const CellParamIndexOutOfBoundsTestParam &testParameter) {
    return os << "CellParamIndexOutOfBoundsTestParam{expectedMessage=" << testParameter.expectedExceptionMessage <<
        ",actualMessage=" << testParameter.actualException.what() << '}';
}

#endif