#ifndef CELL_PARAM_INDEX_OUT_OF_BOUNDS_TEST_HPP
#define CELL_PARAM_INDEX_OUT_OF_BOUNDS_TEST_HPP

#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "samp-sdk/amx/amx.h"
#include "samp-sdk/exception/cell-param-index-out-of-bounds.hpp"
#include "params/cell-param-index-out-of-bounds-test-param.hpp"
#include <string>
#include <vector>
#include <algorithm>

using namespace sampsdk;

class CellParamIndexOutOfBoundsTest : public ::testing::TestWithParam<CellParamIndexOutOfBoundsTestParam> {


    public:

        CellParamIndexOutOfBoundsTest() {}

        ~CellParamIndexOutOfBoundsTest() {}

        void SetUp() override {}

        void TearDown() override {}

};

TEST_P(CellParamIndexOutOfBoundsTest, ShouldMatchExpectedExceptionMessage) {
    CellParamIndexOutOfBoundsTestParam testParameter = GetParam();

    std::string actualMessage = testParameter.actualException.what();

    ASSERT_THAT(actualMessage, ::testing::Eq(testParameter.expectedExceptionMessage));
}

INSTANTIATE_TEST_SUITE_P(CellParamIndexOutOfBoundsTestGroup, CellParamIndexOutOfBoundsTest,
    ::testing::Values(
        CellParamIndexOutOfBoundsTestParam({}, 0, "Attempt to access invalid index in cell parameter array: paramsCount=0,paramIndex=0,cellParams={}"),
        CellParamIndexOutOfBoundsTestParam({23, 44, 88, 97, 55}, 5, "Attempt to access invalid index in cell parameter array: paramsCount=5,paramIndex=5,cellParams={23,44,88,97,55}"),
        CellParamIndexOutOfBoundsTestParam({23, 44}, 8, "Attempt to access invalid index in cell parameter array: paramsCount=2,paramIndex=8,cellParams={23,44}")
    ));

#endif