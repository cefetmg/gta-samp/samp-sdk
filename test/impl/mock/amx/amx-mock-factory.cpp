#include "mock/amx/amx-mock-factory.hpp"

namespace amxmock {

AMXMock & get_instance() {
    static AMXMock amxMock;
    return amxMock;
}

}