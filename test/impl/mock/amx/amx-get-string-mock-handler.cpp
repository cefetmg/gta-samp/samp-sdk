#include "mock/amx/amx-get-string-mock-handler.hpp"
#include <assert.h>
#include <string.h>

using namespace amxmock;

AMXGetStringMockHandler::AMXGetStringMockHandler(const std::string t_expectedString, const cell *t_stringAddress, const size_t t_stringSize):
    AMXGetStringMockHandler(t_expectedString, t_stringAddress, t_stringSize, AMX_ERR_NONE) {}

AMXGetStringMockHandler::AMXGetStringMockHandler(const std::string t_expectedString, const cell *t_stringAddress, const size_t t_stringSize, int t_statusCode):
    AMXApiMockHandler(t_statusCode), m_expectedString(t_expectedString), m_stringAddress(t_stringAddress), m_expectedUseWchar(0), m_stringSize(t_stringSize) {}

int AMXAPI AMXGetStringMockHandler::operator () (char *t_destinationString, const cell *t_stringAddress, int t_userWchar, size_t t_stringSize) const {
    assert(t_stringAddress == m_stringAddress);
    assert(t_userWchar == m_expectedUseWchar);
    assert(t_stringSize == m_stringSize);
    strcpy(t_destinationString, m_expectedString.c_str());
    return statusCode();
}