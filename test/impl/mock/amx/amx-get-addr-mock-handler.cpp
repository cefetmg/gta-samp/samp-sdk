#include "mock/amx/amx-get-addr-mock-handler.hpp"
#include <assert.h>

using namespace amxmock;

AMXGetAddrMockHandler::AMXGetAddrMockHandler(AMX *t_amxContext, cell t_expectedAddress, cell *t_physicalAddress): 
    AMXGetAddrMockHandler(t_amxContext, t_expectedAddress, t_physicalAddress, AMX_ERR_NONE) {}

AMXGetAddrMockHandler::AMXGetAddrMockHandler(AMX *t_amxContext, cell t_expectedAddress, cell *t_physicalAddress, int t_statusCode): 
    AMXApiMockHandler(t_statusCode), m_amxContext(t_amxContext), m_expectedAddress(t_expectedAddress), m_physicalAddress(t_physicalAddress) {}

int AMXAPI AMXGetAddrMockHandler::operator () (AMX *t_amxContext, cell t_expectedAddress, cell **t_physicalAddress) const {
    assert(m_amxContext == t_amxContext);
    assert(m_expectedAddress == t_expectedAddress);
    *t_physicalAddress = m_physicalAddress;
    return statusCode();
}