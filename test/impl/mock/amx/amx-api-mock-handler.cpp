#include "mock/amx/amx-api-mock-handler.hpp"
#include "samp-sdk/amx/amx.h"

using namespace amxmock;

AMXApiMockHandler::AMXApiMockHandler(int t_statusCode): m_statusCode(t_statusCode) {}

AMXApiMockHandler::AMXApiMockHandler(): AMXApiMockHandler(AMX_ERR_NONE) {}

int AMXApiMockHandler::statusCode() const { return m_statusCode; }