#include "mock/amx/amx-mock.hpp"
#include "mock/amx/amx-mock-factory.hpp"

using namespace amxmock;

using ::testing::Mock;

AMXMock::AMXMock() { }

uint16_t * AMXAPI amx_Align16(uint16_t *v) {
	return get_instance().amx_Align16(v);
}

uint32_t * AMXAPI amx_Align32(uint32_t *v) {
	return get_instance().amx_Align32(v);
}

#if defined _I64_MAX || defined HAVE_I64
typedef   uint64_t *  AMXAPI (*amx_Align64_t)(uint64_t *v);
  uint64_t * AMXAPI amx_Align64(uint64_t *v) {
	return get_instance().amx_Align64(v);
}
#endif

int AMXAPI amx_Allot(AMX *amx, int cells, cell *amx_addr, cell **phys_addr) {
	return get_instance().amx_Allot(amx, cells, amx_addr, phys_addr);
}

int AMXAPI amx_Callback(AMX *amx, cell index, cell *result, cell *params) {
	return get_instance().amx_Callback(amx, index, result, params);
}

int AMXAPI amx_Cleanup(AMX *amx) {
	return get_instance().amx_Cleanup(amx);
}

int AMXAPI amx_Clone(AMX *amxClone, AMX *amxSource, void *data) {
	return get_instance().amx_Clone(amxClone, amxSource, data);
}

int AMXAPI amx_Exec(AMX *amx, cell *retval, int index) {
	return get_instance().amx_Exec(amx, retval, index);
}

int AMXAPI amx_FindNative(AMX *amx, const char *name, int *index) {
	return get_instance().amx_FindNative(amx, name, index);
}

int AMXAPI amx_FindPublic(AMX *amx, const char *funcname, int *index) {
	return get_instance().amx_FindPublic(amx, funcname, index);
}

int AMXAPI amx_FindPubVar(AMX *amx, const char *varname, cell *amx_addr) {
	return get_instance().amx_FindPubVar(amx, varname, amx_addr);
}

int AMXAPI amx_FindTagId(AMX *amx, cell tag_id, char *tagname) {
	return get_instance().amx_FindTagId(amx, tag_id, tagname);
}

int AMXAPI amx_Flags(AMX *amx,uint16_t *flags) {
	return get_instance().amx_Flags(amx, flags);
}

int AMXAPI amx_GetAddr(AMX *amx,cell amx_addr,cell **phys_addr) {
	return get_instance().amx_GetAddr(amx, amx_addr, phys_addr);
}

int AMXAPI amx_GetNative(AMX *amx, int index, char *funcname) {
	return get_instance().amx_GetNative(amx, index, funcname);
}

int AMXAPI amx_GetPublic(AMX *amx, int index, char *funcname) {
	return get_instance().amx_GetPublic(amx, index, funcname);
}

int AMXAPI amx_GetPubVar(AMX *amx, int index, char *varname, cell *amx_addr) {
	return get_instance().amx_GetPubVar(amx, index, varname, amx_addr);
}

int AMXAPI amx_GetString(char *dest,const cell *source, int use_wchar, size_t size) {
	return get_instance().amx_GetString(dest, source, use_wchar, size);
}

int AMXAPI amx_GetTag(AMX *amx, int index, char *tagname, cell *tag_id) {
	return get_instance().amx_GetTag(amx, index, tagname, tag_id);
}

int AMXAPI amx_GetUserData(AMX *amx, long tag, void **ptr) {
	return get_instance().amx_GetUserData(amx, tag, ptr);
}

int AMXAPI amx_Init(AMX *amx, void *program) {
	return get_instance().amx_Init(amx, program);
}

int AMXAPI amx_InitJIT(AMX *amx, void *reloc_table, void *native_code) {
	return get_instance().amx_InitJIT(amx, reloc_table, native_code);
}

int AMXAPI amx_MemInfo(AMX *amx, long *codesize, long *datasize, long *stackheap) {
	return get_instance().amx_MemInfo(amx, codesize, datasize, stackheap);
}

int AMXAPI amx_NameLength(AMX *amx, int *length) {
	return get_instance().amx_NameLength(amx, length);
}

AMX_NATIVE_INFO * AMXAPI amx_NativeInfo(const char *name, AMX_NATIVE func) {
	return get_instance().amx_NativeInfo(name, func);
}

int AMXAPI amx_NumNatives(AMX *amx, int *number) {
	return get_instance().amx_NumNatives(amx, number);
}

int AMXAPI amx_NumPublics(AMX *amx, int *number) {
	return get_instance().amx_NumPublics(amx, number);
}

int AMXAPI amx_NumPubVars(AMX *amx, int *number) {
	return get_instance().amx_NumPubVars(amx, number);
}

int AMXAPI amx_NumTags(AMX *amx, int *number) {
	return get_instance().amx_NumTags(amx, number);
}

int AMXAPI amx_Push(AMX *amx, cell value) {
	return get_instance().amx_Push(amx, value);
}

int AMXAPI amx_PushArray(AMX *amx, cell *amx_addr, cell **phys_addr, const cell array[], int numcells) {
	return get_instance().amx_PushArray(amx, amx_addr, phys_addr, array, numcells);
}

int AMXAPI amx_PushString(AMX *amx, cell *amx_addr, cell **phys_addr, const char *string, int pack, int use_wchar) {
	return get_instance().amx_PushString(amx, amx_addr, phys_addr, string, pack, use_wchar);
}

int AMXAPI amx_RaiseError(AMX *amx, int error) {
	return get_instance().amx_RaiseError(amx, error);
}

int AMXAPI amx_Register(AMX *amx, const AMX_NATIVE_INFO *nativelist, int number) {
	return get_instance().amx_Register(amx, nativelist, number);
}

int AMXAPI amx_Release(AMX *amx, cell amx_addr) {
	return get_instance().amx_Release(amx, amx_addr);
}

int AMXAPI amx_SetCallback(AMX *amx, AMX_CALLBACK callback) {
	return get_instance().amx_SetCallback(amx, callback);
}

int AMXAPI amx_SetDebugHook(AMX *amx, AMX_DEBUG debug) {
	return get_instance().amx_SetDebugHook(amx, debug);
}

int AMXAPI amx_SetString(cell *dest, const char *source, int pack, int use_wchar, size_t size) {
	return get_instance().amx_SetString(dest, source, pack, use_wchar, size);
}

int AMXAPI amx_SetUserData(AMX *amx, long tag, void *ptr) {
	return get_instance().amx_SetUserData(amx, tag, ptr);
}

int AMXAPI amx_StrLen(const cell *cstring, int *length) {
	return get_instance().amx_StrLen(cstring, length);
}

int AMXAPI amx_UTF8Check(const char *string, int *length) {
	return get_instance().amx_UTF8Check(string, length);
}

int AMXAPI amx_UTF8Get(const char *string, const char **endptr, cell *value) {
	return get_instance().amx_UTF8Get(string, endptr, value);
}

int AMXAPI amx_UTF8Len(const cell *cstr, int *length) {
	return get_instance().amx_UTF8Len(cstr, length);
}

int AMXAPI amx_UTF8Put(char *string, char **endptr, int maxchars, cell value) {
	return get_instance().amx_UTF8Put(string, endptr, maxchars, value);
}