#include "mock/amx/amx-str-len-mock-handler.hpp"
#include <assert.h>

using namespace amxmock;

AMXStrLenMockHandler::AMXStrLenMockHandler(const cell *t_stringAddress, const int t_expectedLength): 
    AMXStrLenMockHandler(t_stringAddress, t_expectedLength, AMX_ERR_NONE) {}

AMXStrLenMockHandler::AMXStrLenMockHandler(const cell *t_stringAddress, const int t_expectedLength, int t_statusCode): 
    AMXApiMockHandler(t_statusCode), m_stringAddress(t_stringAddress), m_expectedLength(t_expectedLength) {}

int AMXAPI AMXStrLenMockHandler::operator () (const cell *t_stringAddress, int *t_targetLength) const {
    assert(m_stringAddress == t_stringAddress);
    *t_targetLength = m_expectedLength;
    return statusCode();
}