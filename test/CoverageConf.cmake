if(UNIX)

  if(APPLE)
    find_library(COVERAGE_LIB "profile_rt")
  else()
    set(COVERAGE_LIB "gcov")
  endif()

  if(NOT COVERAGE_LIB)

    message(
      WARNING "None coverage lib was found. Coverage will not be measured.")

  else()

    message(
      STATUS
        "Compiling from UNIX platform. Code coverage will be measured by ${COVERAGE_LIB}"
    )
    list(APPEND GTEST_MAIN_DEPENDENCIES ${COVERAGE_LIB})
    list(APPEND GTEST_MOCK_DEPENDENCIES ${COVERAGE_LIB})

    list(APPEND COVERAGE_COMPILER_FLAGS "-fprofile-arcs" "-ftest-coverage")
    message(
      STATUS
        "Appending code coverage compiler flags: ${COVERAGE_COMPILER_FLAGS}")
    add_compile_options(${COVERAGE_COMPILER_FLAGS})

  endif()
else()
  message(WARNING "Cannot build with coverage on non-UNIX platform")
endif()
