#include "gtest/gtest.h"
#include "mock/amx/amx-test.hpp"
#include "exception/cell-param-index-out-of-bounds-test.hpp"
#include "function/standard-amx-functional-facade-test.hpp"
#include "io/standard-amx-cell-input-stream-test.hpp"
#include "io/amx-native-parameter-input-cell-stream-test.hpp"
#include "io/cell-input-stream-interface-test.hpp"

int main(int argc, char *argv[]) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
